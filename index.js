let number = Number(prompt("Please input a number"))

console.log("The number you provided is: " + number)

for(let x = number; x > 0; x--) {

    if(x % 10 === 0){
        console.log("The number is divisible by 10. skipping the number")
        continue
    }
    if(x % 5 === 0) {
        console.log(x)
        continue
    }
    if(x <= 50) {
        console.log("The current value is at 50. Terminating Loop")
        break
    }
 
}

let str = "supercalifragilisticexpialidocious"

let consonants = ""

for (let x = 0; x < str.length; x++) {

  if(str[x] == "a" || str[x] == "e" || str[x] == "i" || str[x] == "o" || str[x] == "u"){
    continue
  }
  consonants = consonants + str[x]
}

console.log(str)
console.log(consonants)
